aewm (1.3.12-6) unstable; urgency=medium

  * QA upload.
  * Fix ftbfs with GCC-10. (Closes: #956983)
  * Update compat level to 13.

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Fri, 10 Jul 2020 19:58:05 +0100

aewm (1.3.12-5) unstable; urgency=medium

  * QA upload.
  * Update Standards-Version to 4.5.0
  * Fix FTCBFS. (Closes: #933233)
  * Add Vcs link for salsa.

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Mon, 17 Feb 2020 23:04:48 +0000

aewm (1.3.12-4) unstable; urgency=medium

  * QA Upload.
  * debian/compat: removed.
  * debian/control:
      - Homepage updated.
      - Migrated DH level to 12.
      - Standards-Version updated to 4.4.0.
  * debian/copyright:
      - Full updated.
      - Https added in Source and Format.
      - Old fields removed.
      - Source updated.
  * debian/patches/40_man-spelling-fix.patch: spelling fixed.
  * debian/tests/control: test added (CI).
  * debian/upstream/metadata: metadata added.
  * debian/watch:
      - Https added.
      - Updated to version 4.

 -- Daniel Pimentel <d4n1@d4n1.org>  Tue, 23 Jul 2019 11:29:53 -0300

aewm (1.3.12-3) unstable; urgency=low

  * QA upload.
    - Move to packaging format "3.0 (quilt)".
    - Use hardened build flags
      http://wiki.debian.org/ReleaseGoals/SecurityHardeningBuildFlags
  * debian/aewm.{postrm,postinst,prerm}
    - Lintian fix maintainer-script-without-set-e.
  * debian/compat
    - Update to 9.
  * debian/control
    - (Breaks): rename from Conflicts. Lintian fix conflicts-with-version.
    - (Build-Depends): update to debhelper 9.
    - (Homepage): New field.
    - (Maintainer): Set to QA group.
    - (Standards-Version): Update to 3.9.4.
  * debian/copyright
    - Update to format 1.0.
  * debian/patches
    - (05): New. Contains changes to original sources.
    - (10): New. Do not strip binaries (Closes: #436379).
    - (20): New. Use hardened build flags.
    - (30): New. Fix hyphens in manual page.
  * debian/rules
    - Update to dh(1).
  * debian/source/format
    - New file.
  * debian/watch
    - New file.

 -- Jari Aalto <jari.aalto@cante.net>  Sat, 02 Mar 2013 13:39:50 +0200

aewm (1.3.12-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Makefile: Added "-lX11".  Closes: #553631.
  * Fixed debhelper-but-no-misc-depends.
  * Fixed description-synopsis-starts-with-article.
  * Fixed menu-item-uses-windowmanagers-section.

 -- Bart Martens <bartm@debian.org>  Sun, 16 Oct 2011 01:38:16 +0200

aewm (1.3.12-2) unstable; urgency=low

  * Remove aemenu and aepanel alternatives. aewm 2.0 will use only GTK; Xaw
    will return in 1.4.

 -- Decklin Foster <decklin@red-bean.com>  Thu, 27 Dec 2007 20:50:02 -0500

aewm (1.3.12-1) unstable; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Thu, 27 Dec 2007 13:16:24 -0500

aewm (1.3.11-1) unstable; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Sat, 27 Oct 2007 23:35:02 -0400

aewm (1.3.10-1) unstable; urgency=low

  * New upstream release
  * Updated debian/copyright.

 -- Decklin Foster <decklin@red-bean.com>  Mon, 11 Sep 2006 20:53:07 -0400

aewm (1.3.9-1) unstable; urgency=low

  * New upstream release
  * Remove kill menu item.
  * Update Standards-Version to 3.7.2.

 -- Decklin Foster <decklin@red-bean.com>  Thu, 13 Jul 2006 15:42:47 -0400

aewm (1.3.8-1) unstable; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Fri, 21 Apr 2006 18:15:04 -0400

aewm (1.3.7-1) unstable; urgency=low

  * New upstream release
  * Removed lesstif build-dep.
  * Upload to unstable.

 -- Decklin Foster <decklin@red-bean.com>  Mon, 17 Apr 2006 23:05:12 -0400

aewm (1.3.6-1) experimental; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Sun, 16 Apr 2006 16:03:28 -0400

aewm (1.3.5-1) experimental; urgency=low

  * New upstream release
  * Update build-deps for modular and Xft.

 -- Decklin Foster <decklin@red-bean.com>  Sat, 15 Apr 2006 02:36:27 -0400

aewm (1.3.4-1) experimental; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Tue, 14 Feb 2006 12:02:21 -0500

aewm (1.3.2-2) experimental; urgency=low

  * Build against libxaw7 instead of libxaw8.

 -- Decklin Foster <decklin@red-bean.com>  Tue, 10 Jan 2006 14:29:23 -0500

aewm (1.3.2-1) experimental; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Thu, 10 Nov 2005 11:13:07 -0500

aewm (1.2.6-1) unstable; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Wed,  9 Nov 2005 12:46:16 -0500

aewm (1.2.5-5) unstable; urgency=low

  * /usr/sbin/install-menu -> /usr/bin/install-menu for menu transition.

 -- Decklin Foster <decklin@red-bean.com>  Thu,  6 Oct 2005 16:52:32 -0400

aewm (1.2.5-4) unstable; urgency=low

  * Add all the applicable modular X.org -dev packages to build-depends.

 -- Decklin Foster <decklin@red-bean.com>  Mon, 15 Aug 2005 11:33:27 -0400

aewm (1.2.5-3) unstable; urgency=low

  * Remove all alternatives that we install on purge, not just
    x-window-manager. (Closes: #319715)
  * Build with libxaw8 instead of libxaw7.
  * Bump Standards-Version to 3.6.2.

 -- Decklin Foster <decklin@red-bean.com>  Sat,  6 Aug 2005 12:52:31 -0400

aewm (1.2.5-2) unstable; urgency=low

  * Only call update-alternatives in prerm if we are removing.

 -- Decklin Foster <decklin@red-bean.com>  Mon, 22 Nov 2004 10:29:15 -0500

aewm (1.2.5-1) unstable; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Mon, 15 Nov 2004 10:39:16 -0500

aewm (1.2.4-1) unstable; urgency=low

  * New upstream release
    - My own fix for the menu issues (Closes: #193966, #187082)
    - Rewrote the parser
  * Included Bill Allombert's new menu-method. Thanks for the NMU, Bill!
    (Closes: #251609)
  * Bumped Standards-Version to 3.6.1.

 -- Decklin Foster <decklin@red-bean.com>  Sat,  2 Oct 2004 08:37:07 -0400

aewm (1.2.3-2.1) unstable; urgency=low

  * NMU
  * Apply patch by Erik de Castro Lopo and Ken Caldwell
    to fix aewm menus. (Closes: #193966, #187082).
    In particular this patch allows iconified windows to be deiconified.
  * debian/aewm.menu: Quote fields properly and  capitalize title.
  * lib/parser.h: Fix the parser to treat \x as x for all x, not just ".
    This is necessary to allow to write a string ending by \.
  * debian/aewm.menu-method: use term(),title() and quote fields properly.

 -- Bill Allombert <ballombe@debian.org>  Fri,  7 May 2004 16:39:16 +0200

aewm (1.2.3-2) unstable; urgency=low

  * Use rm -f in postrm (Closes: #197973)
  * Output UTF-8 in menu method (Closes: #183689)
  * Drop menu wm support for now (Closes: #193728)

 -- Decklin Foster <decklin@red-bean.com>  Fri, 18 Jul 2003 13:47:04 -0400

aewm (1.2.3-1) unstable; urgency=low

  * New upstream release
  * Build-Depend on gtk 2.0 instead of 1.2

 -- Decklin Foster <decklin@red-bean.com>  Thu, 27 Mar 2003 22:57:23 -0500

aewm (1.2.2-3) unstable; urgency=low

  * The "next time, test on the *build* machine" release
  * Fix typo in menu method and clean up functions
    - Prevent wm entries from killing X accidentally
  * Really use x-terminal-emulator instead of xterm

 -- Decklin Foster <decklin@red-bean.com>  Thu,  2 Jan 2003 19:22:14 -0500

aewm (1.2.2-2) unstable; urgency=low

  * Edited menu method a bit
    - Removed superfluous "Debian" nesting level
    - Use pidof instead of skill, as procps is not Essential
    - Reduce alternatives priority to 40, wm doesn't really work right
  * Fix DEB_BUILD_OPTIONS support

 -- Decklin Foster <decklin@red-bean.com>  Thu,  2 Jan 2003 00:58:06 -0500

aewm (1.2.2-1) unstable; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Sun, 24 Nov 2002 19:27:04 -0500

aewm (1.2.1-1) unstable; urgency=low

  * New upstream release
  * Use debhelper v4 and policy 3.5.8

 -- Decklin Foster <decklin@red-bean.com>  Sat, 23 Nov 2002 19:13:27 -0500

aewm (1.2.0-1) unstable; urgency=low

  * New upstream release
  * Fixed menu method to use 'include', added Suggests: menu
  * Changed spelling in description (Closes: #124405)

 -- Decklin Foster <decklin@red-bean.com>  Fri,  1 Feb 2002 15:20:29 -0500

aewm (1.1.5-1) unstable; urgency=low

  * New upstream release.
  * Removed comment cruft from menu method.
  * Added DESIGN to debian/aewm.docs.

 -- Decklin Foster <decklin@red-bean.com>  Thu, 13 Dec 2001 13:25:40 -0500

aewm (1.1.4-1) unstable; urgency=low

  * New upstream release.
  * Fixed menu method treewalk order.
  * Increased alternatives priority again (we can restart now).

 -- Decklin Foster <decklin@red-bean.com>  Wed, 12 Dec 2001 16:13:02 -0500

aewm (1.1.3-1) unstable; urgency=low

  * New upstream release
  * Cleaned out build log from .diff.gz
  * Included menu method; bumped alternatives priority accordingly.
  * aepanel/aemenu managed with alternatives.
  * Standards-Version 3.5.6
  * Fixed debconf versioned build-dep (to >=3.0)

 -- Decklin Foster <decklin@red-bean.com>  Tue, 11 Dec 2001 11:21:53 -0500

aewm (1.1.2-2) unstable; urgency=low

  * Add Build-Depends on libxaw7-dev (Closes: #104764)
  * Use debhelper v3 and policy 3.5.5.
  * Sorta support DEB_BUILD_OPTIONS. I need to make some upstream
    changes (there will be a new release soon) to get this to work
    properly. A menu method should be on the way as well.

 -- Decklin Foster <decklin@red-bean.com>  Fri, 20 Jul 2001 12:51:27 -0400

aewm (1.1.2-1) unstable; urgency=low

  * New upstream release
  * Install files into locations compilant with the FHS instead of
    /usr/X11R6.

 -- Decklin Foster <decklin@red-bean.com>  Wed, 10 Jan 2001 15:03:09 -0500

aewm (1.1.1-1) unstable; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Tue,  2 Jan 2001 00:46:36 -0500

aewm (1.0.0-1) unstable; urgency=low

  * New upstream release.
  * Moved update-alternatives call from postrm to prerm.

 -- Decklin Foster <decklin@red-bean.com>  Sun, 31 Dec 2000 12:12:46 -0500

aewm (0.9.19-1) unstable; urgency=low

  * New upstream release
  * Changed "xterm" to "x-terminal-emulator". I had made a note in the
    README about this but somehow I forgot to actually do it.
  * Updated debian/copyright
  * Fixed x-window-manager alternative handling to include man page.

 -- Decklin Foster <decklin@red-bean.com>  Tue,  7 Nov 2000 12:36:56 -0500

aewm (0.9.18-1) unstable; urgency=low

  * New upstream version.
  * Added "Provides: x-window-manager" to control file, and updated
    Standards-Version as appropriate.
  * Updated debhelper rules to v2.

 -- Decklin Foster <decklin@red-bean.com>  Tue, 31 Oct 2000 12:15:36 -0500

aewm (0.9.17-2) unstable; urgency=low

  * Fixed dependency on xf4. Yes, I am stupid.

 -- Decklin Foster <decklin@red-bean.com>  Mon, 25 Sep 2000 20:52:38 -0400

aewm (0.9.17-1) unstable; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Mon, 25 Sep 2000 11:43:00 -0400

aewm (0.9.16-2) unstable; urgency=low

  * fixed Build-Depends typo (Closes: #70922)

 -- Decklin Foster <decklin@red-bean.com>  Tue,  5 Sep 2000 10:01:07 -0400

aewm (0.9.16-1) unstable; urgency=low

  * New upstream release
  * debian/control: added Build-Depends

 -- Decklin Foster <decklin@red-bean.com>  Thu, 31 Aug 2000 16:51:25 -0400

aewm (0.9.15-1) unstable; urgency=low

  * Initial Release.

 -- Decklin Foster <decklin@red-bean.com>  Fri, 28 Jul 2000 15:26:05 -0400
